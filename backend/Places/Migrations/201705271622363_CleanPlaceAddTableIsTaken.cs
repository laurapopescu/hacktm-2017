namespace Places.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CleanPlaceAddTableIsTaken : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tables", "IsTaken", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Tables", "IsTaken");
        }
    }
}
