// <auto-generated />
namespace Places.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class AddCapacity : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddCapacity));
        
        string IMigrationMetadata.Id
        {
            get { return "201705270833047_AddCapacity"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
