namespace Places.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLogo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Places", "Logo", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Places", "Logo");
        }
    }
}
