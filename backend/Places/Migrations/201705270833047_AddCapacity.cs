namespace Places.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCapacity : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Places", "Capacity", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Places", "Capacity");
        }
    }
}
