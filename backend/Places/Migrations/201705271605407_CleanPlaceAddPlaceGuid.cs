namespace Places.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CleanPlaceAddPlaceGuid : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Places", "Address", c => c.String());
            AddColumn("dbo.Places", "Email", c => c.String());
            AddColumn("dbo.Tables", "PlaceGuid", c => c.Guid(nullable: false));
            DropColumn("dbo.Places", "TableId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Places", "TableId", c => c.Int(nullable: false));
            DropColumn("dbo.Tables", "PlaceGuid");
            DropColumn("dbo.Places", "Email");
            DropColumn("dbo.Places", "Address");
        }
    }
}
