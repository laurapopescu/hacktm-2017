namespace Places.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IdToGuid : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Places");
            AddColumn("dbo.Places", "Guid", c => c.Guid(nullable: false));
            AddPrimaryKey("dbo.Places", "Guid");
            DropColumn("dbo.Places", "Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Places", "Id", c => c.Int(nullable: false, identity: true));
            DropPrimaryKey("dbo.Places");
            DropColumn("dbo.Places", "Guid");
            AddPrimaryKey("dbo.Places", "Id");
        }
    }
}
