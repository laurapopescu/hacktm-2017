namespace Places.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class cleanTable : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Places", "FreeTables");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Places", "FreeTables", c => c.Int(nullable: false));
        }
    }
}
