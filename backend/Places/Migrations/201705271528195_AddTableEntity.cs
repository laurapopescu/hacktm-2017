namespace Places.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTableEntity : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Tables",
                c => new
                    {
                        Guid = c.Guid(nullable: false),
                        Seats = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Guid);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Tables");
        }
    }
}
