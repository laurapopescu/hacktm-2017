namespace Places.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTableId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Places", "TableId", c => c.Int(nullable: false));
            DropColumn("dbo.Places", "Address");
            DropColumn("dbo.Places", "Capacity");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Places", "Capacity", c => c.Int(nullable: false));
            AddColumn("dbo.Places", "Address", c => c.String());
            DropColumn("dbo.Places", "TableId");
        }
    }
}
