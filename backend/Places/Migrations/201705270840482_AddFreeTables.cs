namespace Places.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFreeTables : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Places", "FreeTables", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Places", "FreeTables");
        }
    }
}
