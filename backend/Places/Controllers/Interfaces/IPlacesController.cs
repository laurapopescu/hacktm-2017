﻿using Places.DataModels;
using Places.Models;
using System;
using System.Web.Http;

namespace Places.Controllers.Interfaces
{
    public interface IPlacesController
    {
        IHttpActionResult Places();
        IHttpActionResult Place(Guid guid);
        IHttpActionResult AddPlace(Place place);
        IHttpActionResult RemovePlace(Guid guid);
        IHttpActionResult TakeTable(TableRequest takeTable);
        IHttpActionResult FreeTable(TableRequest freeTable);
        IHttpActionResult UpdateTables(UpdateTablesRequest newTables);
    }
}