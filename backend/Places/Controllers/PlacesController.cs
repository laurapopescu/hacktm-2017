﻿using Places.Controllers.Interfaces;
using Places.DataModels;
using Places.Models;
using Places.Repository;
using Places.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Places.Controllers
{
    public class PlacesController : ApiController, IPlacesController
    {
        private static readonly PlacesContext Context = new PlacesContext();

        [AllowAnonymous]
        [HttpGet]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public IHttpActionResult Places()
        {
            var placesList = new List<PlaceResponseItem>();
            var places = Context.Places.ToList();

            foreach (var place in places)
            {
                var tables = Context.Tables.Where(t => t.PlaceGuid == place.Guid && !t.IsTaken).ToList();
                var tableItems = tables.Select(s => new TableResponseItem
                {
                    Seats = s.Seats
                }).DistinctBy(s => s.Seats);

                placesList.Add(new PlaceResponseItem
                {
                    Guid = place.Guid,
                    Address = place.Address,
                    Email = place.Email,
                    Name = place.Name,
                    Logo = place.Logo,
                    EmptySeats = tables.Sum(t => t.Seats),
                    Tables = tableItems
                });
            }

            return Ok(new PlacesResponse
            {
                Places = placesList,
                TotalPlaces = places.Count,
                TotalEmptySeats = Context.Tables.Where(t => !t.IsTaken).Sum(x => x.Seats)
            });
        }

        [AllowAnonymous]
        [HttpGet]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public IHttpActionResult Place(Guid guid)
        {
            var place = Context.Places.FirstOrDefault(p => p.Guid == guid);
            if (place == null)
                return NotFound();

            return Ok(place);
        }

        [AllowAnonymous]
        [HttpPost]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public IHttpActionResult AddPlace([FromBody] Place place)
        {
            Context.Places.Add(place);
            place.Guid = Guid.NewGuid();
            Context.SaveChanges();

            return Ok(place);
        }

        [AllowAnonymous]
        [HttpPost]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public IHttpActionResult RemovePlace(Guid guid)
        {
            var place = Context.Places.SingleOrDefault(p => p.Guid == guid);
            if (place == null)
                return NotFound();

            Context.Places.Remove(place);
            Context.SaveChanges();

            return Ok();
        }

        [AllowAnonymous]
        [HttpPost]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public IHttpActionResult TakeTable([FromBody] TableRequest takeTable)
        {
            var place = Context.Places.SingleOrDefault(p => p.Guid == takeTable.PlaceGuid);
            if (place == null)
                return NotFound();

            var table = Context.Tables.FirstOrDefault(t => t.PlaceGuid == place.Guid &&
                                                           t.Seats == takeTable.SeatsPerTable &&
                                                           !t.IsTaken);
            if (table == null)
                return NotFound();

            table.IsTaken = true;

            Context.SaveChanges();
            return Ok(table);
        }

        [AllowAnonymous]
        [HttpPost]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public IHttpActionResult FreeTable(TableRequest freeTable)
        {
            var place = Context.Places.SingleOrDefault(p => p.Guid == freeTable.PlaceGuid);
            if (place == null)
                return NotFound();

            var table = Context.Tables.FirstOrDefault(t => t.PlaceGuid == place.Guid &&
                                                           t.Seats == freeTable.SeatsPerTable &&
                                                           t.IsTaken);
            if (table == null)
                return NotFound();

            table.IsTaken = false;

            Context.SaveChanges();
            return Ok(table);

        }

        [AllowAnonymous]
        [HttpPost]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public IHttpActionResult UpdateTables(UpdateTablesRequest request)
        {
            var tablesToRemove = Context.Tables.Where(t => t.PlaceGuid == request.PlaceGuid);
            foreach (var table in tablesToRemove)
            {
                Context.Tables.Remove(table);
            }

            foreach (var tableItem in request.Tables)
            {
                var tablesCount = tableItem.NumberOfTables;
                while (tablesCount > 0)
                {
                    var createTable = new Table
                    {
                        Guid = Guid.NewGuid(),
                        Seats = tableItem.SeatsPerTable,
                        PlaceGuid = request.PlaceGuid,
                        IsTaken = false
                    };

                    Context.Tables.Add(createTable);
                    tablesCount--;
                }
            }

            Context.SaveChanges();
            return Ok();
        }

        [AllowAnonymous]
        [HttpPost]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public IHttpActionResult AddTables([FromBody] AddOrRemoveTableRequest request)
        {
            var place = Context.Places.SingleOrDefault(p => p.Guid == request.PlaceGuid);
            if (place == null)
                return NotFound();

            var tablesCount = request.Tables;
            while (tablesCount > 0)
            {
                var createTable = new Table
                {
                    Guid = Guid.NewGuid(),
                    Seats = request.SeatsPerTable,
                    PlaceGuid = request.PlaceGuid,
                    IsTaken = false
                };

                Context.Tables.Add(createTable);
                tablesCount--;
            }

            Context.SaveChanges();
            return Ok();
        }

        [AllowAnonymous]
        [HttpPost]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public IHttpActionResult RemoveTables([FromBody] AddOrRemoveTableRequest request)
        {
            var place = Context.Places.SingleOrDefault(p => p.Guid == request.PlaceGuid);
            if (place == null)
                return NotFound();

            var tablesCount = request.Tables;
            while (tablesCount > 0)
            {
                var table = Context.Tables.FirstOrDefault(t => t.PlaceGuid == request.PlaceGuid
                                                               && t.Seats == request.SeatsPerTable);
                if (table != null)
                {
                    Context.Tables.Remove(table);
                    Context.SaveChanges();
                    tablesCount--;
                }
            }

            return Ok();
        }
    }
}
