﻿using Microsoft.AspNet.Identity;
using Places.Management;
using Places.Models;
using System.Threading.Tasks;
using System.Web.Http;

namespace Places.Controllers
{
    public class AccountController : ApiController
    {
        private readonly AuthManagement _management;

        public AccountController()
        {
            _management = new AuthManagement();
        }

        // POST api/Account/Register
        [AllowAnonymous]
        [HttpPost]
        public async Task<IHttpActionResult> Register([FromBody]User request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = await _management.RegisterUser(request);
            var errorResult = GetErrorResult(result);
            return errorResult ?? Ok();
        }

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _management.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}
