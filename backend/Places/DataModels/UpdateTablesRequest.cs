﻿using System;
using System.Collections.Generic;

namespace Places.DataModels
{
    public class UpdateTablesRequest
    {
        public Guid PlaceGuid { get; set; }
        public IEnumerable<TableItem> Tables { get; set; }
    }

    public class TableItem
    {
        public int NumberOfTables { get; set; }
        public int SeatsPerTable { get; set; }
    }
}