﻿using System;

namespace Places.DataModels
{
    public class TableRequest
    {
        public Guid PlaceGuid { get; set; }
        public int SeatsPerTable { get; set; }
    }

    public class AddOrRemoveTableRequest
    {
        public Guid PlaceGuid { get; set; }
        public int SeatsPerTable { get; set; }
        public int Tables { get; set; }
    }
}