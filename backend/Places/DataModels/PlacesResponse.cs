﻿using System.Collections.Generic;

namespace Places.DataModels
{
    public class PlacesResponse
    {
        public int TotalPlaces { get; set; }
        public int TotalEmptySeats { get; set; }
        public List<PlaceResponseItem> Places { get; set; }
    }
}