﻿using System;
using System.Collections.Generic;

namespace Places.DataModels
{
    public class PlaceResponseItem
    {
        public Guid Guid { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string Logo { get; set; }
        public int EmptySeats { get; set; }
        public IEnumerable<TableResponseItem> Tables { get; set; }
    }
}