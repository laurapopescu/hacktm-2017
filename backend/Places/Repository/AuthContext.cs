﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace Places.Repository
{
    public class AuthContext : IdentityDbContext<IdentityUser>
    {
        public AuthContext() : base("AuthContext")
        {
        }

    }
}