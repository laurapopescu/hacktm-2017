﻿using Places.Models;
using System.Data.Entity;

namespace Places.Repository
{
    public class PlacesContext : DbContext
    {
        public DbSet<Place> Places { get; set; }
        public DbSet<Table> Tables { get; set; }
    }
}