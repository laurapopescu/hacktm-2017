﻿using Microsoft.AspNet.SignalR;
using Places.Models;

namespace Places.Signalr
{
    public class PlacesHub : Hub
    {
        public void Send(string name, string message)
        {
            // Call the broadcastMessage method to update clients.
            Clients.All.broadcastMessage(name, message);
        }
    }
}