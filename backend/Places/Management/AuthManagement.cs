﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Places.Models;
using System;
using System.Threading.Tasks;
using Places.Repository;

namespace Places.Management
{
    public class AuthManagement : IDisposable
    {
        private readonly AuthContext _context;

        private readonly UserManager<IdentityUser> _userManager;

        public AuthManagement()
        {
            _context = new AuthContext();
            _userManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>(_context));
        }

        public async Task<IdentityResult> RegisterUser(User request)
        {
            var user = new IdentityUser
            {
                UserName = request.UserName
            };

            var result = await _userManager.CreateAsync(user, request.Password);
            return result;
        }

        public async Task<IdentityUser> FindUser(string userName, string password)
        {
            IdentityUser user = await _userManager.FindAsync(userName, password);

            return user;
        }

        public void Dispose()
        {
            _context.Dispose();
            _userManager.Dispose();

        }
    }
}