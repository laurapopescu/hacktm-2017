﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Places.Models
{
    public class Table
    {
        [Key]
        public Guid Guid { get; set; }
        public int Seats { get; set; }
        public Guid PlaceGuid { get; set; }
        public bool IsTaken { get; set; }
    }
}