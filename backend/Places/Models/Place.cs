﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Places.Models
{
    public class Place
    {
        [Key]
        public Guid Guid { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string Logo { get; set; }
    }
}