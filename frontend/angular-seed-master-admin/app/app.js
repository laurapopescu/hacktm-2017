var myApp = angular.module('myApp', ['ngAnimate', 'ngAria', 'ui.bootstrap', 'ngMaterial', 'ngMessages', 'ngRoute', 'ui.router']);

myApp.controller('Controller', ['$scope', '$http', function($scope, $http) {
    $scope.oneAtATime = true;
    $scope.isOpen = false;

    var seats = $scope.Seats;
    var tables = $scope.Tables;

    $scope.addTables = function() {
      var tableData = {
                PlaceGuid: '12c43132-cd4a-4bc6-b4ae-e2a6bb0e26b2',
                SeatsPerTable: $scope.Seats, 
                Tables: $scope.Tables
            };
      var sendData = JSON.stringify(tableData);
        $http({
            method: 'POST',
            url: 'http://localhost:49919/api/Places/AddTables',
            headers: {
                'Content-Type': 'application/json', 
                'Accept': 'application/json' 
            },
            data: sendData
        });
    }

    $scope.removeTables = function() {
      var tableData = {
                PlaceGuid: '12c43132-cd4a-4bc6-b4ae-e2a6bb0e26b2',
                SeatsPerTable: $scope.Seats, 
                Tables: $scope.Tables
            };
      var sendData = JSON.stringify(tableData);
        $http({
            method: 'POST',
            url: 'http://localhost:49919/api/Places/RemoveTables',
            headers: {
                'Content-Type': 'application/json', 
                'Accept': 'application/json' 
            },
            data: sendData
        });
    }


    $scope.takeTable = function() {
      var tableData = {
                PlaceGuid: '12c43132-cd4a-4bc6-b4ae-e2a6bb0e26b2',
                SeatsPerTable: $scope.SeatsPerTable
            };
      var sendData = JSON.stringify(tableData);
        $http({
            method: 'POST',
            url: 'http://localhost:49919/api/Places/TakeTable',
            headers: {
                'Content-Type': 'application/json', 
                'Accept': 'application/json' 
            },
            data: sendData
        });
    }

    $scope.freeTable = function() {
            var tableData = {
                PlaceGuid: '12c43132-cd4a-4bc6-b4ae-e2a6bb0e26b2',
                SeatsPerTable: $scope.SeatsPerTable
            };
      var sendData = JSON.stringify(tableData);
        $http({
            method: 'POST',
            url: 'http://localhost:49919/api/Places/FreeTable',
            headers: {
                'Content-Type': 'application/json', 
                'Accept': 'application/json' 
            },
            data: sendData
        });
    }
}]);